import React, { Component } from 'react';
import './Components.css';
import Osoby from '../components/Osoby/Osoby'
import Firmy from '../components/Firmy/Firmy'
import FirmyByPresses from '../components/Firmy/FirmyByPresses'
import OsobyByFirma from './Osoby/OsobyByFirma'
import Statistic from '../components/Statistic/Statistic'
import axios from 'axios'


class View extends Component {


  state = {
    osoby: [],
    firmy: [],
    firmyByPresses: [],
    osobyByFirma: []
    
  }

  componentDidMount(){
    axios.get('http://localhost:8080/api/osoby', {
      auth: {
        username: 'user',
         password: 'password'
        },
      }).then(response =>
    {
      const osoby = response.data;
      this.setState({osoby: osoby})
    });

    axios.get('http://localhost:8080/api/firmy/statistic', {
      auth: {
        username: 'user',
         password: 'password'
        },
      }).then(response =>
    {
      const firmy = response.data;
      this.setState({firmy: firmy})
    });
  }

  showFirmyByPressesAtoZ = (id) =>{
    axios.get('http://localhost:8080/api/osoby/byPresses/' + id, {
      auth: {
        username: 'user',
         password: 'password'
        },
      }).then(response =>
    {
      const osoby = response.data;
      osoby.sort(function(a,b){
        var A = a.nazwaFirmy
        var B = b.nazwaFirmy
        if(A < B){
          return -1;
        }
        if(A > B){
          return 1;
        }
        return 0;

      });
      this.setState({firmyByPresses: osoby});
    }
    );
    window.scrollTo(150, 0);
  }

  showFirmyByPressesZtoA = (id) =>{
    axios.get('http://localhost:8080/api/osoby/byPresses/' + id, {
      auth: {
        username: 'user',
         password: 'password'
        },
      }).then(response =>
    {
      const osoby = response.data;
      osoby.sort(function(a,b){
        var A = a.nazwaFirmy
        var B = b.nazwaFirmy
        if(A < B){
          return 1;
        }
        if(A > B){
          return -1;
        }
        return 0;

      });
      this.setState({firmyByPresses: osoby});
    }
    );
    window.scrollTo(150, 0);
  }

  showOsobyByFirma = (id) =>{
    axios.get('http://localhost:8080/api/firmy/byFirma/' + id, {
      auth: {
        username: 'user',
         password: 'password'
        },
      }).then(response =>
    {
      this.setState({osobyByFirma: response.data});
    }
    );
    window.scrollTo(150, 0);
  }



  render() {



    let selectedPresses = null
    let selectedFirma = null

   if(this.state.selectedPresses !== null){
     selectedPresses = <div className='LD'><FirmyByPresses firmy={this.state.firmyByPresses} OsobyByFirma={this.showOsobyByFirma}/></div>
   }

   if(this.state.selectedFirma !== null){
    selectedFirma = <div className='RD'><OsobyByFirma osoby={this.state.osobyByFirma} FirmyByPressesAtoZ={this.showFirmyByPressesAtoZ} FirmyByPressesZtoA={this.showFirmyByPressesZtoA}/></div>
  }



    return (
      <div>
        <h1 className='HeaderFont'>View Database</h1>
        {selectedPresses}
        {selectedFirma}
        <Osoby osoby={this.state.osoby} FirmyByPressesAtoZ={this.showFirmyByPressesAtoZ} FirmyByPressesZtoA={this.showFirmyByPressesZtoA}/>
        <Firmy firmy={this.state.firmy} OsobyByFirma={this.showOsobyByFirma}/>
      </div>

    );
  }
}

export default View;