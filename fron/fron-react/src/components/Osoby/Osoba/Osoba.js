import React, { Component } from 'react';
import styles from './Osoba.module.css';
import axios from 'axios'


class Osoba extends Component {

        deleteOsoba = () =>{axios.delete('http://localhost:8080/api/osoby/delete/' + this.props.id, {
            auth: {
              username: 'user',
               password: 'password'
              }
            });
        
        setTimeout(() => {
            window.location.reload()
          }, 1000);

    }

    render(){

        return(
            <div className={styles.Osoba} >
                Imie: {this.props.imie}<br/>
                Nazwisko: {this.props.nazwisko}<br/>
                Email: {this.props.email}<br/>
                <button className={styles.ButtonMenu} onClick={() => this.props.FirmyByPressesAtoZ(this.props.id)} >Firmy A to Z</button>
                <button className={styles.ButtonMenu} onClick={() => this.props.FirmyByPressesZtoA(this.props.id)} >Firmy Z to A</button>
                <button className={styles.ButtonMenu} onClick={e => window.confirm("Are you sure you wish to delete this item?") && this.deleteOsoba(e)}>
                    Delete
                </button>
                </div>
                
        );
        
    }

}
export default Osoba;


