import React, { Component } from 'react';
import styles from './Osoby.module.css';
import Osoba from './Osoba/Osoba';


class Osoby extends Component {

    render() {

        const osoby = this.props.osoby.map((osoba) => {
            return (<Osoba imie={osoba.imie} nazwisko={osoba.nazwisko} email={osoba.email}  FirmyByPressesAtoZ={this.props.FirmyByPressesAtoZ} FirmyByPressesZtoA={this.props.FirmyByPressesZtoA} id={osoba.id}/>);
        });


        return (<div className={styles.OsobyBP}>
            {osoby}
        </div>)


    }

}

export default Osoby;