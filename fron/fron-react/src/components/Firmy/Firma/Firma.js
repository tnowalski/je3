import React, { Component } from 'react';
import styles from './Firma.module.css';
import axios from 'axios'


class Firma extends Component {

    deleteFirma = () =>{axios.delete('http://localhost:8080/api/firmy/delete/' + this.props.id, {
        auth: {
          username: 'user',
           password: 'password'
          }
        });
    
    setTimeout(() => {
        window.location.reload()
      }, 1000);

}

    render(){

        return(
            <div className={styles.Firma} >
                Nazwa: {this.props.nazwaFirmy}<br/>
                Prezes: {this.props.prezesFirmy}<br/>
                Worker Number: {this.props.statistic}<br/>
                <button className={styles.ButtonMenu} onClick={() => this.props.OsobyByFirma(this.props.id)} >Workers</button>
                <button className={styles.ButtonMenu} onClick={e => window.confirm("Are you sure you wish to delete this item?") && this.deleteFirma(e)}>
                    Delete
                </button>
                </div>
        );
    }

}



export default Firma;