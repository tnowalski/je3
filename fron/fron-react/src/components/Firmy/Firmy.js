import React, { Component } from 'react';
import styles from './Firmy.module.css';
import Firma from './Firma/Firma';


class Firmy extends Component {

    render() {

        const firmy = this.props.firmy.map((firma) => {
            return (<Firma nazwaFirmy={firma.nazwaFirmy} prezesFirmy={firma.prezesFirmy} statistic={firma.statistic} OsobyByFirma={this.props.OsobyByFirma} id={firma.id} />);
        });
        return (<div className={styles.Firmy}>
            {firmy}
        </div>)


    }

}

export default Firmy;