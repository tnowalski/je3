import React, { Component } from 'react';
import styles from '../Components.css';

class Stat extends Component {


    render(){

        return(
            <div className='Stat' >
                Year: {this.props.year}<br/>
                Workers Numbers: {this.props.number}
            </div>
                
        );
        
    }

}
export default Stat;


