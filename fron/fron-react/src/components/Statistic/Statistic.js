import React, { Component } from 'react';
import styles from '../Components.css';
import Stat from './Stat';


class Statistic extends Component {

    render() {

        const statistic = this.props.statistic.map((stat) => {
            return (<Stat year={stat.year} number={stat.number}/>);
        });


        return (<div>
            <h1 className='HeaderFont'>Satistic</h1>
            {statistic}
        </div>)


    }

}

export default Statistic;