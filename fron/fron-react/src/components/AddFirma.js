import React, { Component } from 'react';
import './Components.css';
import Osoby from './Osoby/Osoby'
import Firmy from './Firmy/Firmy'
import FirmyByPresses from './Firmy/FirmyByPresses'
import OsobyByFirma from './Osoby/OsobyByFirma'
import axios from 'axios'


class AddOsoba extends Component {
  
  constructor() {
    super();
    this.state = {
      nazwaFirmy: '',
      id: '',
      osoby: []
    };
    this.setNazwa = this.setNazwa.bind(this);
    this.setID = this.setID.bind(this);
  }

  setNazwa(event) {
    this.setState({ nazwaFirmy: event.target.value});
  }

  setID(event) {
    this.setState({ id: event.target.value});
  }

  addFirma = (nazwaFirmy, id) => {
    const firma = 
    {
      nazwaFirmy : nazwaFirmy,
      id: id

    }

    axios.post('http://localhost:8080/api/firmy', firma, {
      auth: {
        username: 'user',
         password: 'password'
        },
      });
  }




  componentDidMount(){
    axios.get('http://localhost:8080/api/osoby', {
      auth: {
        username: 'user',
         password: 'password'
        },
      }).then(response =>
    {
      const osoby = response.data;
      this.setState({osoby: osoby})
    });
  }
  render() {


    return (
      <div >
        <form >
            <label  className='AddFormLable'> Nazwa:</label><br/>
            <input type="text" name="imie"  className='AddFormInput' onChange={this.setNazwa}/><br/>
            <label className='AddFormLable'>Prezes:</label><br/>
            <select className='AddFormLable' onChange={this.setID} >
              {this.state.osoby.map((osoba) => (
                <option value={osoba.id}>{osoba.imie} {osoba.nazwisko} email - {osoba.email}</option>
              ))}
          </select><br/>
            <button  className='AddFormButton' onClick={() => this.addFirma(this.state.nazwaFirmy, this.state.id)}>Add</button>
        </form>
      </div>

    );
  }
}

export default AddOsoba;