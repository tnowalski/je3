import React, { Component } from 'react';
import './Components.css';
import Osoby from '../components/Osoby/Osoby'
import Firmy from '../components/Firmy/Firmy'
import FirmyByPresses from '../components/Firmy/FirmyByPresses'
import OsobyByFirma from '../components/Osoby/OsobyByFirma'
import axios from 'axios'


class AddOsoba extends Component {
  
  constructor() {
    super();
    this.state = {
      imie: '',
      nazwisko: '',
      email: ''
    };
    this.setImie = this.setImie.bind(this);
   this.setNazwisko = this.setNazwisko.bind(this);
    this.setEmail = this.setEmail.bind(this);
  }

  setImie(event) {
    this.setState({ imie: event.target.value});
  }

  setNazwisko(event) {
    this.setState({ nazwisko: event.target.value});
  }

  setEmail(event) {
    this.setState({ email: event.target.value});
  }

  addOsoba = (imie, nazwisko, email) => {
    const osoba = 
    {
      imie : imie,
      nazwisko : nazwisko,
      email : email
    }

    axios.post('http://localhost:8080/api/osoby', osoba, {
      auth: {
        username: 'user',
         password: 'password'
        },
      });
  }
  render() {

    return (
      <div >
        <form >
            <label  className='AddFormLable'> Imie:</label><br/>
            <input type="text" name="imie"  className='AddFormInput' onChange={this.setImie}/><br/>
            <label className='AddFormLable'>Nazwisko:</label><br/>
            <input type="text" name="nazwisko"  className='AddFormInput' onChange={this.setNazwisko}/><br/>
            <label className='AddFormLable'>Email: </label><br/>
            <input type="text" name="email"  className='AddFormInput' onChange={this.setEmail} /><br/>
            <button  className='AddFormButton' onClick={() => this.addOsoba(this.state.imie, this.state.nazwisko, this.state.email)}>Add</button>
        </form>
      </div>

    );
  }
}

export default AddOsoba;

