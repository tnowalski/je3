import React, { Component } from 'react';
import { render } from "react-dom";

import './App.css';
import Osoby from '../components/Osoby/Osoby'
import Firmy from '../components/Firmy/Firmy'
import FirmyByPresses from '../components/Firmy/FirmyByPresses'
import OsobyByFirma from '../components/Osoby/OsobyByFirma'
import View from '../components/View'
import AddOsoba from '../components/AddOsoba'
import AddFirma from '../components/AddFirma'
import Statistic from '../components/Statistic/Statistic'
import axios from 'axios'


class App extends Component {

  constructor() {
    super();
    this.state = {
      statistic: [],
      showHideDemo1: true,
      showHideDemo2: false,
      showHideDemo3: false,
      showHideDemo4: false
    };
    this.hideComponent = this.hideComponent.bind(this);
  }
  componentDidMount(){
    axios.get('http://localhost:8080/api/osoby/stat', {
      auth: {
        username: 'user',
         password: 'password'
        },
      }).then(response =>
    {
      const statistic = response.data;
      this.setState({statistic: statistic})
    });



  }

  

  

  hideComponent(name) {
    this.setState({ showHideDemo1: false });
    this.setState({ showHideDemo2: false });
    this.setState({ showHideDemo3: false });
    this.setState({ showHideDemo4: false });
    switch (name) {
      case "showHideDemo1":
        this.setState({ showHideDemo1: !this.state.showHideDemo1 });
        break;
      case "showHideDemo2":
        this.setState({ showHideDemo2: !this.state.showHideDemo2 });
        break;
      case "showHideDemo3":
        this.setState({ showHideDemo3: !this.state.showHideDemo3 });
        break;
      case "showHideDemo4":
        this.setState({ showHideDemo4: !this.state.showHideDemo4 });
        break;
    }
  }

  render() {
    const { showHideDemo1, showHideDemo2, showHideDemo3, showHideDemo4 } = this.state;

    return (
      <div >
          <div className='Header'>
          <button className='SelectPage' onClick={() => this.hideComponent("showHideDemo1")}>
            View
          </button>
          <button className='SelectPage' onClick={() => this.hideComponent("showHideDemo2")}>
            Add Osoba
          </button>
          <button className='SelectPage' onClick={() => this.hideComponent("showHideDemo3")}>
            Add Firma
          </button>
          <button className='SelectPage' onClick={() => this.hideComponent("showHideDemo4")}>
            Statistic
          </button>
        </div>

      <div className='Body'>
        {showHideDemo1 && <View/>}<br/>
        {showHideDemo2 && <AddOsoba />}
        {showHideDemo3 && <AddFirma />}
        {showHideDemo4 &&<Statistic statistic={this.state.statistic}/>}
      </div>
      </div>
    );
  }
}

export default App;