package com.tomasznowalski.je.Repositorys;

import com.tomasznowalski.je.Models.Osoba;
import com.tomasznowalski.je.Models.User;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Component
public class UserRepository {

    @PersistenceContext
    private EntityManager em;

    public boolean exist(User user)
    {

        return  !(em.createQuery("select u from User u where u.name like '"+ user.getName() + "' and u.password like '" + user.getPassword() + "'", User.class)
                .getResultList().isEmpty());
    }

    @Transactional
    public void add(User user)
    {
        User newUser = new User();
        newUser.setName(user.getName());
        newUser.setPassword(user.getPassword());
        em.persist(newUser);
    }
}

