package com.tomasznowalski.je.Repositorys;

import com.tomasznowalski.je.FirmAddObject;
import com.tomasznowalski.je.Models.Firma;
import com.tomasznowalski.je.Models.Osoba;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Component
public class FirmaRepository {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    OsobaRepository osobaRepository;

    public List<Firma> getFirmy()
    {
        return  em.createQuery("select f from Firma f", Firma.class).getResultList();
    }

    @Transactional
    public void addFirma(FirmAddObject firmAddObject) {
        Firma newFirma = new Firma();
        List<Osoba> listWorkers = new ArrayList<>();
        List<Firma> listFirm;

        System.out.println(firmAddObject.getId());
        newFirma.setNazwaFirmy(firmAddObject.getNazwaFirmy());


        if(em.find(Osoba.class,firmAddObject.getId()) != null)
        {
            System.out.println(firmAddObject.getId());
            Osoba presses = em.find(Osoba.class, firmAddObject.getId());
            newFirma.setPrezesFirmy(presses);

            listWorkers.add(presses);
            newFirma.setListaPracownikow(listWorkers);

            em.persist(newFirma);

            listFirm = presses.getListFirm();
            listFirm.add(newFirma);

            em.merge(presses);

        }

    }


    @Transactional
    public void editFirma(int id, Firma firma) {
        if (em.find(Firma.class, id) != null) {
            Firma oldFirma = em.find(Firma.class, id);
            if (firma.getNazwaFirmy() != null)
                oldFirma.setNazwaFirmy(firma.getNazwaFirmy());
            if (firma.getPrezesFirmy() != null)
                oldFirma.setPrezesFirmy(firma.getPrezesFirmy());

            em.merge(oldFirma);
        }
    }

    @Transactional
    public void deleteFirma(int id){
        if(em.find(Firma.class, id) != null){
                for (Osoba osoba : em.createQuery("select o from Osoba o", Osoba.class).getResultList()) {
                    Osoba newOsoba = osoba;
                    newOsoba.getListFirm().remove(em.find(Firma.class, id));
                    if(newOsoba.getListFirm().isEmpty())
                        newOsoba.setDataZatrudnienia(null);
                    em.merge(newOsoba);
                }
                em.remove(em.find(Firma.class, id));
        }
    }

    public List<Osoba> getWorkerByFirma(int id)
    {
        List<Osoba> listWorker = new ArrayList<>();
        for(Osoba osoba : osobaRepository.getOsoby())
        {
            for(Firma firma : osoba.getListFirm())
            {
                if(firma.getId() == id)
                {
                    listWorker.add(osoba);
                    break;
                }
            }
        }
        return listWorker;
    }



}
