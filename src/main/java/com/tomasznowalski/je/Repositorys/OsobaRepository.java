package com.tomasznowalski.je.Repositorys;

import com.tomasznowalski.je.CSVData;
import com.tomasznowalski.je.Models.Firma;
import com.tomasznowalski.je.Models.Osoba;
import com.tomasznowalski.je.Statistic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Component
public class OsobaRepository {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    FirmaRepository firmaRepository;

    public List<Osoba> getOsoby()
    {
        return  em.createQuery("select o from Osoba o", Osoba.class).getResultList();
    }

    @Transactional
    public void addOsoba(Osoba osoba) {
        Osoba newOsoba = new Osoba();
        List<Firma> listFirma;
        newOsoba.setImie(osoba.getImie());
        newOsoba.setNazwisko(osoba.getNazwisko());
        newOsoba.setEmail(osoba.getEmail());
        newOsoba.setDataZatrudnienia(LocalDate.now());

        if(osoba.getListFirm() != null)
            listFirma = osoba.getListFirm();
        else
            listFirma = new ArrayList<>();

        newOsoba.setListFirm(listFirma);
        em.persist(newOsoba);

        em.persist(newOsoba);
        if(osoba.getListFirm() != null) {
            for (Firma firma : osoba.getListFirm()) {
                Firma newFirma = em.find(Firma.class, firma.getId());
                List<Osoba> listOsob = newFirma.getListaPracownikow();
                listOsob.add(newOsoba);
                newFirma.setListaPracownikow(listOsob);
                em.merge(newFirma);
            }
        }
    }

    @Transactional
    public void editOsoba(int id, Osoba osoba) {
        if(em.find(Osoba.class, id) != null) {
            Osoba oldOsoba = em.find(Osoba.class, id);
            if(osoba.getImie() != null)
                oldOsoba.setImie(osoba.getImie());
            if(osoba.getNazwisko() != null)
                oldOsoba.setNazwisko(osoba.getNazwisko());
            if(osoba.getEmail() != null)
                oldOsoba.setEmail(osoba.getEmail());
            if(osoba.getDataZatrudnienia() != null)
                oldOsoba.setDataZatrudnienia(osoba.getDataZatrudnienia());
        /*    if (osoba.getFirma() != null)
                oldOsoba.setFirma(osoba.getFirma());
*/
            em.merge(oldOsoba);
        }
    }

    @Transactional
    public void deleteOsoba(int id){
        if(em.find(Osoba.class, id) != null) {
            for(Firma firma : em.createQuery("select f from Firma f", Firma.class).getResultList()) {
                Firma newFirma = firma;
                if(firma.getPrezesFirmy() != null)
                    if(firma.getPrezesFirmy().getId() == id)
                        newFirma.setPrezesFirmy(null);
                List<Osoba> listWorker = firma.getListaPracownikow();
                listWorker.remove(em.find(Osoba.class, id));
                newFirma.setListaPracownikow(listWorker);
                em.merge(newFirma);
            }

            em.remove(em.find(Osoba.class, id));
        }
    }

    public List<Firma> getFirmyByPresses(int id)
    {
        return  em.createQuery("select f from Firma f where f.prezesFirmy.id like " + id, Firma.class).getResultList();
    }

    public List<Statistic> getStat(){
        List<Statistic> listStat = new ArrayList<>();
        int counter = 0;
        for(int year = 2010; year <= 2021; year++){
            for(Osoba worker : em.createQuery("select o from Osoba o where o.dataZatrudnienia is not null", Osoba.class).getResultList()){
                if(worker.getDataZatrudnienia().getYear() == year)
                    counter++;
            }
            listStat.add(new Statistic(year, counter));
            counter = 0;
        }
        return listStat;
    }

    @Transactional
    public void addCSV(List<CSVData> csvDataList)
    {
        for (CSVData csvData : csvDataList) {
            Osoba newOsoba = new Osoba();
            newOsoba.setImie(csvData.getFirstName());
            newOsoba.setNazwisko(csvData.getLastName());
            newOsoba.setEmail(csvData.getEmail());
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/y", Locale.ENGLISH);
            newOsoba.setDataZatrudnienia(LocalDate.parse(csvData.getDateOfEmployment(), formatter));

            List<Firma> listFirmy = em.createQuery("select f from Firma f where f.nazwaFirmy like '" + csvData.getCompany()  + "'", Firma.class).getResultList();

            if(listFirmy.isEmpty())
            {
                Firma newFirma = new Firma();
                newFirma.setNazwaFirmy(csvData.getCompany());
                newFirma.setPrezesFirmy(newOsoba);
                List<Osoba> listWorkers = new ArrayList<>();
                listWorkers.add(newOsoba);
                newFirma.setListaPracownikow(listWorkers);
                em.persist(newFirma);

                List<Firma> listFirm = new ArrayList<>();
                listFirm.add(newFirma);
                newOsoba.setListFirm(listFirm);
                em.persist(newOsoba);
            }
            else {
                Firma existingFirma = em.createQuery("select f from Firma f where f.nazwaFirmy like '" + csvData.getCompany()  + "'", Firma.class).getSingleResult();
                List<Osoba> listWorkers = existingFirma.getListaPracownikow();
                listWorkers.add(newOsoba);
                existingFirma.setListaPracownikow(listWorkers);

                List<Firma> listFirm = new ArrayList<>();
                listFirm.add(existingFirma);
                newOsoba.setListFirm(listFirm);
                em.persist(newOsoba);
                em.merge(existingFirma);
            }

        }
    }

}
