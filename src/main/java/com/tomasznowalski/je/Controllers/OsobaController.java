package com.tomasznowalski.je.Controllers;

import com.tomasznowalski.je.CSVData;
import com.tomasznowalski.je.FirmaStatisticObject;
import com.tomasznowalski.je.Models.Firma;
import com.tomasznowalski.je.Models.Osoba;
import com.tomasznowalski.je.Servieces.OsobaService;
import com.tomasznowalski.je.Statistic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/osoby")
public class OsobaController {

    @Autowired
    OsobaService osobaService;

    @CrossOrigin
    @GetMapping
    public List<Osoba> getOsoby()
    {
        return osobaService.getOsoby();
    }

    @CrossOrigin
    @PostMapping
    public void addOsoba(@RequestBody Osoba osoba)
    {
       osobaService.addOsoba(osoba);
    }

    @CrossOrigin
    @PatchMapping("/edit/{id}")
    public void editOsoba(@PathVariable int id, @RequestBody Osoba osoba)
    {
        osobaService.editOsoba(id, osoba);
    }

    @CrossOrigin
    @DeleteMapping("/delete/{id}")
    public void deleteOsoba(@PathVariable int id)
    {
        osobaService.deleteOsoba(id);
    }

    @CrossOrigin
    @GetMapping("/byPresses/{id}")
    public List<FirmaStatisticObject> getFirmyByPresses(@PathVariable int id){
        List<FirmaStatisticObject> listFirm = new ArrayList<>();
        for(Firma firma : osobaService.getFirmyByPresses(id))
            listFirm.add(new FirmaStatisticObject(firma.getId(), firma.getNazwaFirmy(), firma.getListaPracownikow(), firma.getPrezesFirmy(), firma.getListaPracownikow().size()));
        return listFirm;

    }

    @CrossOrigin
    @GetMapping("/stat")
    public List<Statistic> getStat(){
        return osobaService.getStat();
    }


    /**/

    @PostMapping("/addList")
    public void addCSV(@RequestBody List<CSVData> csvDataList)
    {
        osobaService.addCSV(csvDataList);
    }

}
