package com.tomasznowalski.je.Controllers;

import com.tomasznowalski.je.FirmAddObject;
import com.tomasznowalski.je.FirmaStatisticObject;
import com.tomasznowalski.je.Models.Firma;
import com.tomasznowalski.je.Models.Osoba;
import com.tomasznowalski.je.Servieces.FirmaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/firmy")
public class FirmaController {

    @Autowired
    FirmaService firmaService;

    @CrossOrigin
    @GetMapping
    public List<Firma> getFirma()
    {
        return firmaService.getFirmy();
    }

    @CrossOrigin
    @GetMapping("/statistic")
    public List<FirmaStatisticObject> getFirmaStatistic()
    {
        List<FirmaStatisticObject> listFirm = new ArrayList<>();
        for(Firma firma : firmaService.getFirmy())
            listFirm.add(new FirmaStatisticObject(firma.getId(), firma.getNazwaFirmy(), firma.getListaPracownikow(), firma.getPrezesFirmy(), firma.getListaPracownikow().size()));
        return listFirm;
    }

    @CrossOrigin
    @PostMapping
    public void addFirma(@RequestBody FirmAddObject firmAddObject)
    {
        firmaService.addFirma(firmAddObject);
    }

    @CrossOrigin
    @PatchMapping("/edit/{id}")
    public void editFirma(@PathVariable int id, @RequestBody Firma firma)
    {
        firmaService.editFirma(id, firma);
    }

    @CrossOrigin
    @DeleteMapping("/delete/{id}")
    public void deleteFirma(@PathVariable int id)
    {
        firmaService.deleteFirma(id);
    }

    @CrossOrigin
    @GetMapping("/byFirma/{id}")
    public List<Osoba> getWorkerByFirma(@PathVariable int id)
    {
        return firmaService.getWorkerByFirma(id);
    }




}