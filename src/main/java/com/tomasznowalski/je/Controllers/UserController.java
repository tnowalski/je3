package com.tomasznowalski.je.Controllers;

import com.tomasznowalski.je.CSVData;
import com.tomasznowalski.je.FirmaStatisticObject;
import com.tomasznowalski.je.Models.Firma;
import com.tomasznowalski.je.Models.Osoba;
import com.tomasznowalski.je.Models.User;
import com.tomasznowalski.je.Servieces.OsobaService;
import com.tomasznowalski.je.Servieces.UserServices;
import com.tomasznowalski.je.Statistic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
@RestController
@RequestMapping("api/auth")
public class UserController {

    @Autowired
    UserServices userServices;

    @CrossOrigin
    @GetMapping
    public boolean exist(@RequestBody User user)
    {
        return userServices.exist(user);
    }

    @CrossOrigin
    @PostMapping
    public void add(@RequestBody User user)
    {
        userServices.add(user);
    }

}