package com.tomasznowalski.je.Servieces;

import com.tomasznowalski.je.CSVData;
import com.tomasznowalski.je.Models.Firma;
import com.tomasznowalski.je.Models.Osoba;
import com.tomasznowalski.je.Repositorys.OsobaRepository;
import com.tomasznowalski.je.Statistic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OsobaService {

    @Autowired
    OsobaRepository osobaRepository;

    public List<Osoba> getOsoby() {
        return  osobaRepository.getOsoby();
    }

    public void addOsoba(Osoba osoba) {
        osobaRepository.addOsoba(osoba);
    }

    public void editOsoba(int id, Osoba osoba)
    {
        osobaRepository.editOsoba(id, osoba);
    }

    public void deleteOsoba(int id){
        osobaRepository.deleteOsoba(id);
    }

    public List<Firma> getFirmyByPresses(int id){
        return osobaRepository.getFirmyByPresses(id);
    }
    public List<Statistic> getStat(){
        return osobaRepository.getStat();
    }

    public void addCSV(List<CSVData> csvDataList)
    {
        osobaRepository.addCSV(csvDataList);
    }

}
