package com.tomasznowalski.je.Servieces;

import com.tomasznowalski.je.FirmAddObject;
import com.tomasznowalski.je.Models.Firma;
import com.tomasznowalski.je.Models.Osoba;
import com.tomasznowalski.je.Repositorys.FirmaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FirmaService {

    @Autowired
    FirmaRepository firmaRepository;

    public List<Firma> getFirmy()
    {
        return  firmaRepository.getFirmy();
    }

    public void addFirma(FirmAddObject firmAddObject)
    {
        firmaRepository.addFirma(firmAddObject);
    }

    public void editFirma(int id, Firma firma)
    {
        firmaRepository.editFirma(id, firma);
    }

    public void deleteFirma(int id){
        firmaRepository.deleteFirma(id);
    }

    public List<Osoba> getWorkerByFirma(int id){
        return firmaRepository.getWorkerByFirma(id);
    }


}
