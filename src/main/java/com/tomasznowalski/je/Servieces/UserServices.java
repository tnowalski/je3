package com.tomasznowalski.je.Servieces;

import com.tomasznowalski.je.Models.User;
import com.tomasznowalski.je.Repositorys.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServices {

    @Autowired
    UserRepository userRepository;

    public boolean exist(User user)
    {
        return userRepository.exist(user);
    }

    public void add(User user)
    {
         userRepository.add(user);
    }
}
