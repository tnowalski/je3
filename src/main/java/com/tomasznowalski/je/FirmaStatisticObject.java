package com.tomasznowalski.je;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.tomasznowalski.je.Models.Osoba;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.List;

@AllArgsConstructor
@Setter
@Getter
public class FirmaStatisticObject {

    private int id;
    private String nazwaFirmy;
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "nazwisko")
    @JsonIdentityReference
    private List<Osoba> listaPracownikow;
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "nazwisko")
    @JsonIdentityReference
    private Osoba prezesFirmy;
    private int statistic;
}
