package com.tomasznowalski.je;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
public class FirmAddObject {
    private String nazwaFirmy;
    private int id;
}
