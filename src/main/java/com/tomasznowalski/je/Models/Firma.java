package com.tomasznowalski.je.Models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
public class Firma {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NonNull
    private String nazwaFirmy;


    @ManyToMany
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "nazwisko")
    @JsonIdentityReference
    private List<Osoba> listaPracownikow;
   @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "nazwisko")
    @JsonIdentityReference
    @ManyToOne
    private Osoba prezesFirmy;


    @Override
    public String toString() {
        if (prezesFirmy != null)
            return "Firma{" +
                    "nazwaFirmy='" + nazwaFirmy + '\'' +
                    ", prezesFirmy= id:" + prezesFirmy.getId() +
                    '}';
        else
            return "Firma{" +
                    "nazwaFirmy='" + nazwaFirmy + '\'' +
                    ", prezesFirmy=brak" +
                    '}';
    }
}
