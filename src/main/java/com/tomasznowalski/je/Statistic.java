package com.tomasznowalski.je;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Statistic {
    private int year;
    private int number;
}
