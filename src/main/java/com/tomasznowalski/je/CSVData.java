package com.tomasznowalski.je;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CSVData {

    private String firstName;
    private String lastName;
    private String email;
    private String company;
    private String dateOfEmployment;
}
